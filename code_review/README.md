Before getting started on this code review exercise you'll need to set up a Merge Request on Gitlab:
- Create a new branch for the `code_review` exercise and commit the files under the `code_review` directory (`Elements` and `ManageTeam`).
- Open an MR for your branch and add your comments to the MR as you would if you were reviewing code changes submitted by a teammate.

Now that your MR is set up, please take some time to familiarize yourself with these files. This is a real example from the Bridge code repository (circa 2018). Please complete a thorough code review of these files, esp considering:
- Would you make any changes to the file naming or structure themselves? Why or why not?
- Can you improve the way data is being passed between these components?

For the purposes of this assignment, assume all json endpoints return exactly the data needed for each component. Please add comments in gitlab directly. Your role is to both improve the underlying code AND to explain why you're suggesting changes, just as you would in a real code review.