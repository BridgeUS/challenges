// Pass in month, abbreviated to 3 letters, and day (2 number date)
// Displays a date component with a small month and larger day, roughly equal width

import React, { Component } from 'react';
import PropTypes from 'prop-types';

const SquareDate = (props) => (
	<div className={`square-date${props.inactive ? " inactive" : ""}`}>
		<div className="square-month all-caps-text">{props.month}</div>
		<div className="square-day">{props.day.length < 2 ? `0${props.day}` : props.day}</div>
	</div>
);

SquareDate.propTypes = {
	month: PropTypes.string.isRequired,
	day: PropTypes.string.isRequired,
	inactive: PropTypes.bool,
};

SquareDate.defaultProps = {
	inactive: false,
};

export default SquareDate;