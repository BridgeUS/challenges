Congratulations! You've made it to the project stage of our interview process.

There are 2 projects included in this repository, which you can learn about under the `code_review` and `pattern_matching` directories. To help make the `code_review` project as realistic as possible, please follow the below steps:

- Create a new project with an empty repository on Gitlab.
- For each project, open the directory and view additional instructions in its README.
- Finally, share your repository with @forrestblount with Maintainer access so I can review and share with additional team members as needed.

If you have any questions or require more time to complete these challenges, don't hesitate to reach out.
